import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

public class Auflöser {

    private HashMap<Character, String> teams;
    private HashMap<Character, String> unterbrechung;
    private HashMap<Character, String> karten;
    private HashMap<Character, String> spieler;
    private HashMap<Character, String> einwechslung;
    private HashMap<Character, String> punktestand1;
    private HashMap<Character, String> punktestand2;






    public Auflöser() {
        this.teams = new HashMap<>();
        this.unterbrechung = new HashMap<>();
        this.karten = new HashMap<>();
        this.spieler = new HashMap<>();
        this.einwechslung = new HashMap<>();
        this.punktestand1 = new HashMap<>();
        this.punktestand2 = new HashMap<>();

        teams.put('1', "Entscheidung für Team 1");
        teams.put('2', "Entscheidung für Team 2");

        unterbrechung.put('A', "Sie erhalten einen Anstoss");
        unterbrechung.put('B', "Sie erhalten einen Strafstoss");
        unterbrechung.put('C', "Sie erhalten einen Eckball");
        unterbrechung.put('D', "Sie erhalten einen Abstoss");
        unterbrechung.put('E', "Sie erhalten einen Freistoss");
        unterbrechung.put('F' , "Sie erhalten einen indirekten Freistoss");
        unterbrechung.put('G', "Sie erhalten einen Vorteil");
        unterbrechung.put('H', "Sie erhalten einen Tor");
        unterbrechung.put('I', "Sie erhalten einen Einwurf");
        unterbrechung.put('J', "Sie erhalten einen Schlusspfiff");
        unterbrechung.put('K', "Sie erhalten einen Freistoss wegen einem Abseits im vorderen Drittel");
        unterbrechung.put('L', "Sie erhalten einen Freistoss wegen einem Abseits im mittleren Drittel");
        unterbrechung.put('M', "Sie erhalten einen Freistoss wegen einem Abseits im letzten Drittel");

        karten.put('0', "Es gibt keine Karte");
        karten.put('1', "Es gibt eine gelbe Karte");
        karten.put('2', "Es gibt eine rote Karte");

        spieler.put('A', "Kein Spieler");
        spieler.put('B', "Für Spieler 1");
        spieler.put('C', "Für Spieler 2");
        spieler.put('D', "Für Spieler 3");
        spieler.put('E', "Für Spieler 4");
        spieler.put('F', "Für Spieler 5");
        spieler.put('G', "Für Spieler 6");
        spieler.put('H', "Für Spieler 7");
        spieler.put('I', "Für Spieler 8");
        spieler.put('J', "Für Spieler 9");
        spieler.put('K', "Für Spieler 10");
        spieler.put('L', "Für Spieler 11");
        spieler.put('M', "Für Spieler 12");
        spieler.put('N', "Für Spieler 13");
        spieler.put('O', "Für Spieler 14");
        spieler.put('P', "Für Spieler 15");

        einwechslung.put('0', "Keine Einwechslung");
        einwechslung.put('1', "Einwechslung bei Team 1");
        einwechslung.put('2', "Einwechslung bei Team 2");

        punktestand1.put('0', "Keine Tore Team 1");
        punktestand1.put('1', "Ein Tor Team 1");
        punktestand1.put('2', "Zwei Tore Team 1");
        punktestand1.put('3', "Drei Tore Team 1");
        punktestand1.put('4', "Vier Tore Team 1");
        punktestand1.put('5', "Fünf Tore Team 1");
        punktestand1.put('6', "Sechs Tore Team 1");
        punktestand1.put('7', "Sieben Tore Team 1");
        punktestand1.put('8', "Acht Tore Team 1");
        punktestand1.put('9', "Neun Tore Team 1");

        punktestand2.put('0', "Keine Tore Team 2");
        punktestand2.put('1', "Ein Tor Team 2");
        punktestand2.put('2', "Zwei Tore Team 2");
        punktestand2.put('3', "Drei Tore Team 2");
        punktestand2.put('4', "Vier Tore Team 2");
        punktestand2.put('5', "Fünf Tore Team 2");
        punktestand2.put('6', "Sechs Tore Team 2");
        punktestand2.put('7', "Sieben Tore Team 2");
        punktestand2.put('8', "Acht Tore Team 2");
        punktestand2.put('9', "Neun Tore Team 2");

    }

    public boolean valid(String code){
        return Pattern.compile("[1-2][A-M][0-2][A-P][0-2][0-9][0-9][0-9][0-9]").matcher(code).matches();
    }

    public void löser(String codee){
        if(valid(codee)){
            System.out.println(teams.get(codee.charAt(0)));
            System.out.println(unterbrechung.get(codee.charAt(1)));
            System.out.println(karten.get(codee.charAt(2)));
            System.out.println(spieler.get(codee.charAt(3)));
            System.out.println(einwechslung.get(codee.charAt(4)));
            System.out.println(punktestand1.get(codee.charAt(5)));
            System.out.println(punktestand2.get(codee.charAt(6)));
            System.out.println("Minute " + codee.charAt(7) + codee.charAt(8));


        } else{
            System.out.println("Ungültiger Code");
        }
        }

    }







//1B1B12982

